#include "turret.h"

turret::turret()
{
	rotation = 0.0f;
	topSpeed = 0.2f;
	speed = 0.0f;
	acceleration = 0.0f;
	momentum = 0.0f;

	rotateLeft = 0;
	rotateRight = 0;
}

bool turret::getRotateLeft(void)
{
	return rotateLeft;
}

bool turret::getRotateRight(void)
{
	return rotateRight;
}

float turret::getThisScaleFactor(void)
{
	return thisScaleFactor;
}

void turret::setRotateLeft(bool state)
{
	rotateLeft = state;
}

void turret::setRotateRight(bool state)
{
	rotateRight = state;
}

void turret::setThisScaleFactor(float sf)
{
	thisScaleFactor = sf;
}

//allows player control of turret
void turret::turretRotation(void)
{
	if (rotateLeft)
	{
		rotation += 1.0f * thisScaleFactor;
	}

	if (rotateRight)
	{
		rotation -= 1.0f * thisScaleFactor;
	}
}