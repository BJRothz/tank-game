#include "movingEntity.h"


movingEntity::movingEntity()
{
	topSpeed = 0.2f;
	speed = 0.0f;
	acceleration = 0.0f;
	momentum = 0.0f;
}

float movingEntity::getTopSpeed(void)
{
	return topSpeed;
}

float movingEntity::getSpeed(void)
{
	return speed;
}

float movingEntity::getAcceleration(void)
{
	return acceleration;
}

float movingEntity::getMomentum(void)
{
	return momentum;
}

void movingEntity::setTopSpeed(float speed)
{
	topSpeed = speed;
}

void movingEntity::setSpeed(float thisSpeed)
{
	speed = thisSpeed;
}

void movingEntity::setAcceleration(float acc)
{
	acceleration = acc;
}

void movingEntity::setMomentum(float mom)
{
	momentum = mom;
}