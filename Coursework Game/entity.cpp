#include "entity.h"


entity::entity()
{
	xCoord = 0.0f;
	yCoord = 0.0f;
	rotation = 0.0f;
}

float entity::getXCoord(void)
{
	return xCoord;
}

float entity::getYCoord(void)
{
	return yCoord;
}

float entity::getRotation(void)
{
	return rotation;
}

void entity::setXCoord(float x)
{
	xCoord = x;
}

void entity::setYCoord(float y)
{
	yCoord = y;
}

void entity::setRotation(float theRotation)
{
	rotation = theRotation;
}