#pragma once
#include <string>
#include "entity.h"

using namespace std;

class movingEntity : public entity
{
public:

	movingEntity();

	//get function prototypes
	float getTopSpeed(void);
	float getSpeed(void);
	float getAcceleration(void);
	float getMomentum(void);

	//set function prototypes
	void setTopSpeed(float speed);
	void setSpeed(float thisSpeed);
	void setAcceleration(float acc);
	void setMomentum(float mom);

protected:

	float topSpeed;
	float speed;
	float acceleration;
	float momentum;


};