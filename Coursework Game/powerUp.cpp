#include "powerUp.h"

powerUp::powerUp()
{
	xCoord = 0;
	yCoord = 0;
	rotation = 0;
}

powerUp::powerUp(float x, float y)
{
	xCoord = x;
	yCoord = y;
}

powerUp::powerUp(float x, float y, int rot)
{
	xCoord = x;
	yCoord = y;
	rotation = rot;
}

int powerUp::getEffectTime(void)
{
	return effectTime;
}

void powerUp::setEffectTime(int x)
{
	effectTime = x;
}