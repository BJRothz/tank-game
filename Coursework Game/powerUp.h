#pragma once
#include <string>
#include <stdlib.h>
#include <time.h>
#include "entity.h"

using namespace std;

class powerUp : public entity
{
public:
	powerUp();
	powerUp(float x, float y);
	powerUp(float x, float y, int rotation);

	//get functions
	int getEffectTime(void);

	//set functions
	void setEffectTime(int x);

protected:

	int effectTime;

};