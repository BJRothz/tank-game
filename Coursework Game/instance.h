#pragma once
#include <string>
#include "tank.h"
#include "powerUp.h"
#include "windows.h"

using namespace std;

class instance
{
public:
	tank * tankPointer = new tank;
	powerUp * powerUp1 = new powerUp;
	powerUp * powerUp2 = new powerUp;
	
	instance();

	~instance();

	void calculateCurrentDelta(void);

	void calculateScaleFactor(void);

	// get function prototypes
	int getNoOfTanks(void);
	DWORD getFrameStartTime(void);
	DWORD getFrameEndTime(void);
	float getCurrentDelta(void);
	float getScaleFactor(void);
	float getIDealDelta(void);

	// set function prototypes
	void setNoOfTanks(int num);
	void setFrameStartTime(DWORD start);
	void setFrameEndTime(DWORD end);
	void setCurrentDelta(float delta);
	void setScaleFactor(float sf);

protected:

	int noOfTanks;
	int idealFPS;
	DWORD frameStartTime;
	DWORD frameEndTime;
	float currentDelta;
	float scaleFactor;
	float idealDelta;

};
