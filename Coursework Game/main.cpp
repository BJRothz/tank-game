

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// OpenGL without using GLUT - 2013 by Neil Dansey, Tim Dykes and Ian Cant, and using excerpts from here:
// http://bobobobo.wordpress.com/2008/02/11/opengl-in-a-proper-windows-app-no-glut/
// Feel free to adapt this for what you need, but please leave these comments in.

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#pragma once

#define _USE_MATH_DEFINES

#include <windows.h>		// need this file if you want to create windows etc
#include <gl/gl.h>			// need this file to do graphics with opengl
#include <gl/glu.h>			// need this file to set up a perspective projection easily
#include <math.h>			// maths file needed for complex mathematical equations
#include "entity.h"			// includes the entity class
#include "movingEntity.h"	//includes the movingEntity class
#include "tank.h"			// includes the tank class
#include "instance.h"		// includes the instance class
#include "turret.h"			// includes the turret class
#include "powerUp.h"		//includes the powerUp class

// include the opengl and glu libraries
#pragma comment(lib, "opengl32.lib")	
#pragma comment(lib, "glu32.lib")

float tankVertices[18] = 
{
	-0.1f, -0.1f, 0.0f,
	0.2f, -0.1f, 0.0f,
	-0.1f, 0.1f, 0.0f,
	-0.1f, 0.1f, 0.0f,
	0.2f, -0.1f, 0.0f,
	0.2f, 0.1f, 0.0f
};
float tankVertexColors[24] = 
{
	0.0f, 1.0f, 0.0f, 1.0f,
	0.0f, 1.0f, 0.0f, 1.0f,
	0.0f, 1.0f, 0.0f, 1.0f,
	0.0f, 1.0f, 0.0f, 1.0f,
	0.0f, 1.0f, 0.0f, 1.0f,
	0.0f, 1.0f, 0.0f, 1.0f
};
float tankVertexColors2[24] =
{
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f
};

float turretVertices[18] =
{
	0.0f, -0.05f, 0.0f,
	0.3f, -0.05f, 0.0f,
	0.0f, 0.05f, 0.0f,
	0.0f, 0.05f, 0.0f,
	0.3f, -0.05f, 0.0f,
	0.3f, 0.05f, 0.0f
};

float turretVertexColors[24] =
{
	1.0f, 0.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 1.0f, 1.0f
};

float turretVertexColors2[24] = 
{
	1.0f, 1.0f, 0.0f, 1.0f,
	1.0f, 1.0f, 0.0f, 1.0f,
	1.0f, 1.0f, 0.0f, 1.0f,
	1.0f, 1.0f, 0.0f, 1.0f,
	1.0f, 1.0f, 0.0f, 1.0f,
	1.0f, 1.0f, 0.0f, 1.0f
};

float powerUpVertices[27] = 
{
	0.0f, 0.0f, 0.0f,
	-0.1f, 0.1f, 0.0f,
	-0.1f, -0.1f, 0.0f,
	0.0f, 0.0f, 0.0f,
	-0.1f, -0.1f, 0.0f,
	0.1f, -0.1f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.1f, -0.1f, 0.0f,
	0.1f, 0.1f, 0.0f

};

float powerUpVertexColors[36] =
{
	0.0f, 1.0f, 1.0f, 1.0f,
	0.0f, 1.0f, 1.0f, 1.0f,
	0.0f, 1.0f, 1.0f, 1.0f,
	0.0f, 1.0f, 1.0f, 1.0f,
	0.0f, 1.0f, 1.0f, 1.0f,
	0.0f, 1.0f, 1.0f, 1.0f,
	0.0f, 1.0f, 1.0f, 1.0f,
	0.0f, 1.0f, 1.0f, 1.0f,
	0.0f, 1.0f, 1.0f, 1.0f
};

float powerUpVertexColors2[36] =
{
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f
};

float bulletVertices[9] =
{
	-0.05f, 0.05f, 1.0f,
	-0.05f, -0.05f, 1.0f,
	0.05f, 0.0f, 1.0f
};

float bulletVertexColors[12] =
{
	0.5f, 0.5f, 0.0f, 1.0f,
	0.5f, 0.5f, 0.0f, 1.0f,
	0.5f, 0.5f, 0.0f, 1.0f
};

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


// function prototypes:
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow);
bool detectCollision(float x1, float x2, float y1, float y2, float threshold);
void update(instance *game);
void draw(instance *game);
void wrap(float coord, float positiveEdge, float negativeEdge);

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// In a C++ Windows app, the starting point is WinMain() rather than _tmain() or main().
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow)
{
	// some basic numbers to hold the position and size of the window
	int windowWidth = 800;
	int windowHeight = 600;
	int windowTopLeftX = 50;
	int windowTopLeftY = 50;

	// some other variables we need for our game...
	MSG msg;								// this will be used to store messages from the operating system
	bool keepPlaying = true;				// whether or not we want to keep playing


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// this section contains all the window initialisation code, 
	// and should probably be collapsed for the time being to avoid confusion	
#pragma region  <-- click the plus/minus sign to collapse/expand!

	// this bit creates a window class, basically a template for the window we will make later, so we can do more windows the same.
	WNDCLASS myWindowClass;
	myWindowClass.cbClsExtra = 0;											// no idea
	myWindowClass.cbWndExtra = 0;											// no idea
	myWindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	// background fill black
	myWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);					// arrow cursor       
	myWindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);				// type of icon to use (default app icon)
	myWindowClass.hInstance = hInstance;									// window instance name (given by the OS when the window is created)   
	myWindowClass.lpfnWndProc = WndProc;									// window callback function - this is our function below that is called whenever something happens
	myWindowClass.lpszClassName = TEXT("my window class name");				// our new window class name
	myWindowClass.lpszMenuName = 0;											// window menu name (0 = default menu?) 
	myWindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;				// redraw if the window is resized horizontally or vertically, allow different context for each window instance

	// Register that class with the Windows OS..
	RegisterClass(&myWindowClass);

	// create a rect structure to hold the dimensions of our window
	RECT rect;
	SetRect(&rect, windowTopLeftX,				// the top-left corner x-coordinate
		windowTopLeftY,				// the top-left corner y-coordinate
		windowTopLeftX + windowWidth,		// far right
		windowTopLeftY + windowHeight);	// far left

	// adjust the window, no idea why.
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, false);

	// call CreateWindow to create the window
	HWND myWindow = CreateWindow(TEXT("my window class name"),		// window class to use - in this case the one we created a minute ago
		TEXT("CT4TOGA Coursework Template"),		// window title
		WS_OVERLAPPEDWINDOW,						// ??
		windowTopLeftX, windowTopLeftY,			// x, y
		windowWidth, windowHeight,				// width and height
		NULL, NULL,								// ??
		hInstance, NULL);							// ??


	// check to see that the window created okay
	if (myWindow == NULL)
	{
		FatalAppExit(NULL, TEXT("CreateWindow() failed!")); // ch15
	}

	// if so, show it
	ShowWindow(myWindow, iCmdShow);


	// get a device context from the window
	HDC myDeviceContext = GetDC(myWindow);


	// we create a pixel format descriptor, to describe our desired pixel format. 
	// we set all of the fields to 0 before we do anything else
	// this is because PIXELFORMATDESCRIPTOR has loads of fields that we won't use
	PIXELFORMATDESCRIPTOR myPfd = { 0 };


	// now set only the fields of the pfd we care about:
	myPfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);		// size of the pfd in memory
	myPfd.nVersion = 1;									// always 1

	myPfd.dwFlags = PFD_SUPPORT_OPENGL |				// OpenGL support - not DirectDraw
		PFD_DOUBLEBUFFER |				// double buffering support
		PFD_DRAW_TO_WINDOW;					// draw to the app window, not to a bitmap image

	myPfd.iPixelType = PFD_TYPE_RGBA;					// red, green, blue, alpha for each pixel
	myPfd.cColorBits = 24;								// 24 bit == 8 bits for red, 8 for green, 8 for blue.
	// This count of color bits EXCLUDES alpha.

	myPfd.cDepthBits = 32;								// 32 bits to measure pixel depth.


	// now we need to choose the closest pixel format to the one we wanted...	
	int chosenPixelFormat = ChoosePixelFormat(myDeviceContext, &myPfd);

	// if windows didnt have a suitable format, 0 would have been returned...
	if (chosenPixelFormat == 0)
	{
		FatalAppExit(NULL, TEXT("ChoosePixelFormat() failed!"));
	}

	// if we get this far it means we've got a valid pixel format
	// so now we need to set the device context up with that format...
	int result = SetPixelFormat(myDeviceContext, chosenPixelFormat, &myPfd);

	// if it failed...
	if (result == NULL)
	{
		FatalAppExit(NULL, TEXT("SetPixelFormat() failed!"));
	}

	// we now need to set up a render context (for opengl) that is compatible with the device context (from windows)...
	HGLRC myRenderContext = wglCreateContext(myDeviceContext);

	// then we connect the two together
	wglMakeCurrent(myDeviceContext, myRenderContext);



	// opengl display setup
	glMatrixMode(GL_PROJECTION);	// select the projection matrix, i.e. the one that controls the "camera"
	glLoadIdentity();				// reset it
	gluPerspective(45.0, (float)windowWidth / (float)windowHeight, 1, 1000);	// set up fov, and near / far clipping planes
	glViewport(0, 0, windowWidth, windowHeight);							// make the viewport cover the whole window
	glClearColor(0.5, 0, 0.5, 1.0);											// set the colour used for clearing the screen

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#pragma endregion

	instance *game = new instance;

	//setup of tank position
	game->tankPointer[0].setXCoord(-2.0f);
	game->tankPointer[1].setXCoord(2.0f);
	game->tankPointer[1].setRotation(180);

	//setup of tank turret position
	game->tankPointer[0].tankTurret.setXCoord(game->tankPointer[0].getXCoord());
	game->tankPointer[0].tankTurret.setYCoord(game->tankPointer[0].getYCoord());

	game->tankPointer[1].tankTurret.setXCoord(game->tankPointer[1].getXCoord());
	game->tankPointer[1].tankTurret.setYCoord(game->tankPointer[1].getYCoord());
	game->tankPointer[1].tankTurret.setRotation(game->tankPointer[1].getRotation());

	// main game loop starts here!


	// keep doing this as long as the player hasnt exited the app: 
	while (keepPlaying == true)
	{

		game->setFrameStartTime(GetTickCount());					//gets the frame start time

		//wrapping tank movement
			//tank 1
		if (game->tankPointer[0].getXCoord() > 2.6f)
		{
			game->tankPointer[0].setXCoord(-2.6f);
		}
		if (game->tankPointer[0].getXCoord() < -2.6f)
		{
			game->tankPointer[0].setXCoord(2.6f);
		}
		if (game->tankPointer[0].getYCoord() > 1.95f)
		{
			game->tankPointer[0].setYCoord(-1.95f);
		}
		if (game->tankPointer[0].getYCoord() < -1.95f)
		{
			game->tankPointer[0].setYCoord(1.95f);
		}
			//tank 2
		if (game->tankPointer[1].getXCoord() > 2.6f)
		{
			game->tankPointer[1].setXCoord(-2.6f);
		}
		if (game->tankPointer[1].getXCoord() < -2.6f)
		{
			game->tankPointer[1].setXCoord(2.6f);
		}
		if (game->tankPointer[1].getYCoord() > 1.95f)
		{
			game->tankPointer[1].setYCoord(-1.95f);
		}
		if (game->tankPointer[1].getYCoord() < -1.95f)
		{
			game->tankPointer[1].setYCoord(1.95f);
		}
		
		//reload bullet

			//tank 1 bullet
		if (game->tankPointer[0].thisBullet->getXCoord() > 2.6f)
		{
			game->tankPointer[0].thisBullet->setXCoord(100);
			game->tankPointer[0].thisBullet->setYCoord(100);
			game->tankPointer[0].thisBullet->setSpeed(0);
			game->tankPointer[0].setReloaded(1);
		}
		if (game->tankPointer[0].thisBullet->getXCoord() < -2.6f)
		{
			game->tankPointer[0].thisBullet->setXCoord(100);
			game->tankPointer[0].thisBullet->setYCoord(100);
			game->tankPointer[0].thisBullet->setSpeed(0);
			game->tankPointer[0].setReloaded(1);
		}
		if (game->tankPointer[0].thisBullet->getYCoord() > 1.95f)
		{
			game->tankPointer[0].thisBullet->setXCoord(100);
			game->tankPointer[0].thisBullet->setYCoord(100);
			game->tankPointer[0].thisBullet->setSpeed(0);
			game->tankPointer[0].setReloaded(1);
		}
		if (game->tankPointer[0].thisBullet->getYCoord() < -1.95f)
		{
			game->tankPointer[0].thisBullet->setXCoord(100);
			game->tankPointer[0].thisBullet->setYCoord(100);
			game->tankPointer[0].thisBullet->setSpeed(0);
			game->tankPointer[0].setReloaded(1);
		}

		//tank 2 bullet
		if (game->tankPointer[1].thisBullet->getXCoord() > 2.6f)
		{
			game->tankPointer[1].thisBullet->setXCoord(100);
			game->tankPointer[1].thisBullet->setYCoord(100);
			game->tankPointer[1].thisBullet->setSpeed(0);
			game->tankPointer[1].setReloaded(1);
		}
		if (game->tankPointer[1].thisBullet->getXCoord() < -2.6f)
		{
			game->tankPointer[1].thisBullet->setXCoord(100);
			game->tankPointer[1].thisBullet->setYCoord(100);
			game->tankPointer[1].thisBullet->setSpeed(0);
			game->tankPointer[1].setReloaded(1);
		}
		if (game->tankPointer[1].thisBullet->getYCoord() > 1.95f)
		{
			game->tankPointer[1].thisBullet->setXCoord(100);
			game->tankPointer[1].thisBullet->setYCoord(100);
			game->tankPointer[1].thisBullet->setSpeed(0);
			game->tankPointer[1].setReloaded(1);
		}
		if (game->tankPointer[1].thisBullet->getYCoord() < -1.95f)
		{
			game->tankPointer[1].thisBullet->setXCoord(100);
			game->tankPointer[1].thisBullet->setYCoord(100);
			game->tankPointer[1].thisBullet->setSpeed(0);
			game->tankPointer[1].setReloaded(1);
		}

		// we need to listen out for OS messages.
		// if there is a windows message to process...
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// and if the message is a "quit" message...
			if (msg.message == WM_QUIT)
			{
				keepPlaying = false;	// we want to quit asap
			}
			else if (msg.message == WM_KEYDOWN)	//key down event handling
			{
				switch (msg.wParam)
				{
				case 27:
					keepPlaying = false;		//esc key is pressed closes application
					break;

				case 87:						//w key pressed makes tank 1 move forward
					game->tankPointer[0].setMoveForward(1);
					break;

				case 83:						//s key pressed makes tank 1 move backward
					game->tankPointer[0].setMoveBackward(1);
					break;

				case 65:						//a key pressed makes tank 1 rotate left
					game->tankPointer[0].setTurnLeft(1);
					break;

				case 70:						//f key pressed makes tank 1 fire
					if (game->tankPointer[0].getReloaded() == 1)
					{
						game->tankPointer[0].setFire(1);
					}
					break;

				case 68:						//d key pressed makes tank 1 rotate right
					game->tankPointer[0].setTurnRight(1);
					break;

				case 81:						//q key rotates player 1 turret left
					game->tankPointer[0].tankTurret.setRotateLeft(1);
					break;

				case 69:						//e key rotates player 1 turret right
					game->tankPointer[0].tankTurret.setRotateRight(1);
					break;

				case 73:						//i pressed makes tank 2 move forward
					game->tankPointer[1].setMoveForward(1);
					break;

				case 75:						//k pressed makes tank 2 move backward
					game->tankPointer[1].setMoveBackward(1);
					break;

				case 74:						//j pressed makes tank 2 rotate left
					game->tankPointer[1].setTurnLeft(1);
					break;

				case 76:						//l pressed makes tank 2 rotate right
					game->tankPointer[1].setTurnRight(1);
					break;

				case 72:						//h key fires tank 2 bullets
					if (game->tankPointer[1].getReloaded() == 1)
					{
						game->tankPointer[1].setFire(1);
					}
					break;

				case 85:						//u key turns player 2 turret left
					game->tankPointer[1].tankTurret.setRotateLeft(1);
					break;

				case 79:						//o key turns player 2 turret right
					game->tankPointer[1].tankTurret.setRotateRight(1);
					break;

				default:

					break;
				}
			}
			else if (msg.message == WM_KEYUP)
			{
				switch (msg.wParam)
				{
				case 27:
					keepPlaying = false;		//esc key is pressed closes application
					break;

				case 87:						//w key pressed makes tank 1 move forward
					game->tankPointer[0].setMoveForward(0);
					break;

				case 83:						//s key pressed makes tank 1 move backward
					game->tankPointer[0].setMoveBackward(0);
					break;

				case 65:						//a key pressed makes tank 1 rotate left
					game->tankPointer[0].setTurnLeft(0);
					break;

				case 68:						//d key pressed makes tank 1 rotate right
					game->tankPointer[0].setTurnRight(0);
					break;

				case 70:						//f key pressed stops tank 1 bullets firing
					game->tankPointer[0].setFire(0);
					break;

				case 81:						//q key stops player 1 turret rotating left
					game->tankPointer[0].tankTurret.setRotateLeft(0);
					break;

				case 69:						//e key stops player 1 turret rotating right
					game->tankPointer[0].tankTurret.setRotateRight(0);
					break;

				case 73:						//i pressed makes tank 2 move forward
					game->tankPointer[1].setMoveForward(0);
					break;

				case 75:					//k pressed makes tank 2 move backward
					game->tankPointer[1].setMoveBackward(0);
					break;

				case 74:					//j pressed makes tank 2 rotate left
					game->tankPointer[1].setTurnLeft(0);
					break;

				case 76:					//l pressed makes tank 2 rotate right
					game->tankPointer[1].setTurnRight(0);
					break;

				case 72:						// h key stops tank 2 bullets firing
					game->tankPointer[1].setFire(0);
					break;

				case 85:						//u key stops player 2 turret rotating left
					game->tankPointer[1].tankTurret.setRotateLeft(0);
					break;

				case 79:						//o key stops player 2 turret rotating right
					game->tankPointer[1].tankTurret.setRotateRight(0);
					break;

				default:

					break;
				}
			}
			// or if it was any other type of message (i.e. one we don't care about), process it as normal...
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}


		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// clear screen

		//collision detection handling - player shot
		bool tank1KO = 0;
		bool tank2KO = 0;
		if (detectCollision(game->tankPointer[0].getXCoord(), game->tankPointer[1].thisBullet->getXCoord(), game->tankPointer[0].getYCoord(), game->tankPointer[1].thisBullet->getYCoord(), 0.2f) && !game->tankPointer[0].getInvincible())
			{
				tank1KO = true;
			}
			else
			{
				tank1KO = false;
			}

			if (detectCollision(game->tankPointer[0].thisBullet->getXCoord(), game->tankPointer[1].getXCoord(), game->tankPointer[0].thisBullet->getYCoord(), game->tankPointer[1].getYCoord(), 0.2f) && !game->tankPointer[1].getInvincible())
			{
				tank2KO = true;
			}
			else
			{
				tank2KO = false;
			}

		//collision detection handling - power ups
		if (detectCollision(game->tankPointer[0].getXCoord(), game->powerUp1->getXCoord(), game->tankPointer[0].getYCoord(), game->powerUp1->getYCoord(), 0.2f))	//speed increase if player 1 touches PowerUp 1
		{
			game->powerUp1->setXCoord(100);
			game->tankPointer[0].setTopSpeed(0.04f);
			game->powerUp1->setEffectTime(GetTickCount() + 5000);
		}

		if (detectCollision(game->tankPointer[1].getXCoord(), game->powerUp1->getXCoord(), game->tankPointer[1].getYCoord(), game->powerUp1->getYCoord(), 0.2f))	//speed increase if player 2 touches PowerUp 1
		{
			game->powerUp1->setXCoord(100);
			game->tankPointer[1].setTopSpeed(0.04f);
			game->powerUp1->setEffectTime(GetTickCount() + 5000);
		}

		if (detectCollision(game->tankPointer[0].getXCoord(), game->powerUp2->getXCoord(), game->tankPointer[0].getYCoord(), game->powerUp2->getYCoord(), 0.2f))	// invincibility if player 1 touches PowerUp 2
		{
			game->powerUp2->setXCoord(100);
			game->tankPointer[0].setInvincible(1);
			game->powerUp2->setEffectTime(GetTickCount() + 5000);
		}

		if (detectCollision(game->tankPointer[1].getXCoord(), game->powerUp2->getXCoord(), game->tankPointer[1].getYCoord(), game->powerUp2->getYCoord(), 0.2f))	//invincibility if player 2 touches PowerUp 2
		{
			game->powerUp2->setXCoord(100);
			game->tankPointer[1].setInvincible(1);
			game->powerUp2->setEffectTime(GetTickCount() + 5000);
		}

			//reset game when a player is killed
		if (tank1KO || tank2KO == 1)
		{
			//reset tank 1 position
			game->tankPointer[0].setXCoord(-2.0f);
			game->tankPointer[0].setYCoord(0.0f);
			game->tankPointer[0].setRotation(0.0f);
			game->tankPointer[0].thisBullet->setXCoord(100);
			game->tankPointer[0].setSpeed(0.0f);

			//reset tank 2 position
			game->tankPointer[1].setXCoord(2.0f);
			game->tankPointer[1].setYCoord(0.0f);
			game->tankPointer[1].setRotation(180);
			game->tankPointer[1].thisBullet->setXCoord(100);
			game->tankPointer[1].setSpeed(0.0f);

			//reset tank 1 turret position
			game->tankPointer[0].tankTurret.setXCoord(game->tankPointer[0].getXCoord());
			game->tankPointer[0].tankTurret.setYCoord(game->tankPointer[0].getYCoord());
			game->tankPointer[0].tankTurret.setRotation(game->tankPointer[0].getRotation());

			//reset tank 2 turret position
			game->tankPointer[1].tankTurret.setXCoord(game->tankPointer[1].getXCoord());
			game->tankPointer[1].tankTurret.setYCoord(game->tankPointer[1].getYCoord());
			game->tankPointer[1].tankTurret.setRotation(game->tankPointer[1].getRotation());

			//reset PowerUps
			srand(time(NULL));

			float ran1 = rand() % 200;
			float ran2 = rand() % 200;
			float ran3 = rand() % 200;
			float ran4 = rand() % 200;
			float ran5 = rand() % 200;

			game->powerUp1->setXCoord((ran1 - 100) * (2.6 / 100));
			game->powerUp1->setYCoord((ran2 - 100) * (1.95 / 100));
			game->powerUp1->setRotation(ran5 * (360 / 100));
			game->powerUp2->setXCoord((ran3 - 100) * (2.6 / 100));
			game->powerUp2->setYCoord((ran4 - 100) * (1.95 / 100));
		}


			//end powerup effects after 5 seconds
		if (game->powerUp1->getEffectTime() == GetTickCount())
		{
			game->tankPointer[0].setTopSpeed(0.02f);
			game->tankPointer[1].setTopSpeed(0.02f);
			//game->powerUp1 = new PowerUp();
		}

		if (game->powerUp2->getEffectTime() == GetTickCount())
		{
			game->tankPointer[0].setInvincible(0);
			game->tankPointer[1].setInvincible(0);
		}

		update(game);											// updates game
		draw(game);												// draws next frame

		SwapBuffers(myDeviceContext);							// update graphics

		game->setFrameEndTime(GetTickCount());					//gets the end time of the frame
		game->calculateCurrentDelta();							//calculates the current delta from start and end frame times
		game->calculateScaleFactor();							//calculates the scale factor from current and ideal deltas
		
	}


	// the next bit will therefore happen when the player quits the app,
	// because they are trapped in the previous section as long as (keepPlaying == true).

	// UNmake our rendering context (make it 'uncurrent')
	wglMakeCurrent(NULL, NULL);

	// delete the rendering context, we no longer need it.
	wglDeleteContext(myRenderContext);

	// release our window's DC from the window
	ReleaseDC(myWindow, myDeviceContext);

	// end the program
	return msg.wParam;

	//cleaning up dynamic variables
	delete game;
	game = NULL;
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// this part contains some code that should be collapsed for now too...
#pragma region keep_this_bit_collapsed_too!

// this function is called when any events happen to our window
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{

	switch (message)
	{
		// if they exited the window...	
	case WM_DESTROY:
		// post a message "quit" message to the main windows loop
		PostQuitMessage(0);
		return 0;
		break;
	}

	// must do this as a default case (i.e. if no other event was handled)...
	return DefWindowProc(hwnd, message, wparam, lparam);

}

#pragma endregion

bool detectCollision(float x1, float x2, float y1, float y2, float threshold)
{
	// gets difference in x and difference in y
	float xDiff = (x2 - x1);
	float yDiff = (y2 - y1);

	// gets the x and y differences squared
	float xDiffSquared = pow(xDiff, 2);
	float yDiffSquared = pow(yDiff, 2);

	// square roots the sum of the x and y differences squared
	float distance = sqrt(xDiffSquared + yDiffSquared);

	// assesses the distance against the collision threshold and outputs true if a collision occurs or false if not
	if (distance <= threshold) { return true; }
	else { return false; }
}

void draw(instance *game)
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glMatrixMode(GL_MODELVIEW);

	//draw tank 1 bullet
	glVertexPointer(3, GL_FLOAT, 0, bulletVertices);
	glColorPointer(4, GL_FLOAT, 0, bulletVertexColors);
	glLoadIdentity();
	glTranslatef(game->tankPointer[0].thisBullet->getXCoord(), game->tankPointer[0].thisBullet->getYCoord(), -6.0);
	glRotatef(game->tankPointer[0].thisBullet->getRotation(), 0.0, 0.0, 1.0);
	glDrawArrays(GL_TRIANGLES, 0, 3);

	//draw tank 2 bullet
	glVertexPointer(3, GL_FLOAT, 0, bulletVertices);
	glColorPointer(4, GL_FLOAT, 0, bulletVertexColors);
	glLoadIdentity();
	glTranslatef(game->tankPointer[1].thisBullet->getXCoord(), game->tankPointer[1].thisBullet->getYCoord(), -6.0);
	glRotatef(game->tankPointer[1].thisBullet->getRotation(), 0.0, 0.0, 1.0);
	glDrawArrays(GL_TRIANGLES, 0, 3);

	// draw player 1...
	glVertexPointer(3, GL_FLOAT, 0, tankVertices);
	glColorPointer(4, GL_FLOAT, 0, tankVertexColors);
	glLoadIdentity();
	glTranslatef(game->tankPointer[0].getXCoord(), game->tankPointer[0].getYCoord(), -5.0);
	glRotatef(game->tankPointer[0].getRotation(), 0.0, 0.0, 1.0);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	//draw player 1 turret
	glVertexPointer(3, GL_FLOAT, 0, turretVertices);
	glColorPointer(4, GL_FLOAT, 0, turretVertexColors);
	glLoadIdentity();
	glTranslatef(game->tankPointer[0].tankTurret.getXCoord(), game->tankPointer[0].tankTurret.getYCoord(), -5.0);
	glRotatef(game->tankPointer[0].tankTurret.getRotation(), 0.0, 0.0, 1.0);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	// draw player 2...
	glVertexPointer(3, GL_FLOAT, 0, tankVertices);
	glColorPointer(4, GL_FLOAT, 0, tankVertexColors2);
	glLoadIdentity();
	glTranslatef(game->tankPointer[1].getXCoord(), game->tankPointer[1].getYCoord(), -5.0);
	glRotatef(game->tankPointer[1].getRotation(), 0.0, 0.0, 1.0);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	//draw player 2 turret
	glVertexPointer(3, GL_FLOAT, 0, turretVertices);
	glColorPointer(4, GL_FLOAT, 0, turretVertexColors2);
	glLoadIdentity();
	glTranslatef(game->tankPointer[1].tankTurret.getXCoord(), game->tankPointer[1].tankTurret.getYCoord(), -5.0);
	glRotatef(game->tankPointer[1].tankTurret.getRotation(), 0.0, 0.0, 1.0);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	//draw powerUp 1
	glVertexPointer(3, GL_FLOAT, 0, powerUpVertices);
	glColorPointer(4, GL_FLOAT, 0, powerUpVertexColors);
	glLoadIdentity();
	glTranslatef(game->powerUp1->getXCoord(), game->powerUp1->getYCoord(), -5.0);
	glRotatef(game->powerUp1->getRotation(), 0.0, 0.0, 1.0);
	glDrawArrays(GL_TRIANGLES, 0, 9);

	//draw powerUp 2
	glVertexPointer(3, GL_FLOAT, 0, powerUpVertices);
	glColorPointer(4, GL_FLOAT, 0, powerUpVertexColors2);
	glLoadIdentity();
	glTranslatef(game->powerUp2->getXCoord(), game->powerUp2->getYCoord(), -5.0);
	glRotatef(game->powerUp2->getRotation(), 0.0, 0.0, 1.0);
	glDrawArrays(GL_TRIANGLES, 0, 9);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}

void update(instance *game)
{
		//tank 1 movement
	//if (game->tankPointer[0].getMoveForward())
	//{
	//	game->tankPointer[0].speed = 0.002f;
	//}

	//if (game->tankPointer[0].getMoveBackward())
	//{
	//	game->tankPointer[0].speed = -0.002f;
	//}

	//if (!game->tankPointer[0].getMoveForward() && !game->tankPointer[0].getMoveBackward())
	//{
	//	game->tankPointer[0].speed = 0.0f;
	//}

	//if (game->tankPointer[0].getTurnRight())
	//{
	//	game->tankPointer[0].setRotation(game->tankPointer[0].getRotation() - 0.5);
	//}

	//if (game->tankPointer[0].getTurnLeft())
	//{
	//	game->tankPointer[0].setRotation(game->tankPointer[0].getRotation() + 0.5);
	//}

	//if (game->tankPointer[0].getFire())
	//{
	//	//fireProjectile();
	//}

		//reset scale factor in tank class
	game->tankPointer[0].setThisScaleFactor(game->getScaleFactor());
	game->tankPointer[1].setThisScaleFactor(game->getScaleFactor());

		//reset scale factor in turret class
	game->tankPointer[0].tankTurret.setThisScaleFactor(game->getScaleFactor());
	game->tankPointer[1].tankTurret.setThisScaleFactor(game->getScaleFactor());

		//calls tank & turret control functions
	game->tankPointer[0].control();
	game->tankPointer[0].tankTurret.turretRotation();
	game->tankPointer[1].control();
	game->tankPointer[1].tankTurret.turretRotation();

		//tank 1 movement
	game->tankPointer[0].setXCoord(game->tankPointer[0].getXCoord() + (game->tankPointer[0].getSpeed() * game->getScaleFactor() * (cos((game->tankPointer[0].getRotation()*M_PI) / 180))));		//x Coord movement with rotation
	game->tankPointer[0].setYCoord(game->tankPointer[0].getYCoord() + (game->tankPointer[0].getSpeed() * game->getScaleFactor() * (sin((game->tankPointer[0].getRotation()*M_PI) / 180))));		//y Coord movement with rotation

		//tank 2 movement
	game->tankPointer[1].setXCoord(game->tankPointer[1].getXCoord() + (game->tankPointer[1].getSpeed() * game->getScaleFactor() * (cos((game->tankPointer[1].getRotation()*M_PI) / 180))));		//x Coord movement with rotation
	game->tankPointer[1].setYCoord(game->tankPointer[1].getYCoord() + (game->tankPointer[1].getSpeed() * game->getScaleFactor() * (sin((game->tankPointer[1].getRotation()*M_PI) / 180))));		//y Coord movement with rotation

		//tank 1 bullet
	game->tankPointer[0].thisBullet->setXCoord(game->tankPointer[0].thisBullet->getXCoord() + (game->tankPointer[0].thisBullet->getSpeed() * game->getScaleFactor() * (cos((game->tankPointer[0].thisBullet->getRotation()*M_PI) / 180)))); //x coord bullet movement with rotation
	game->tankPointer[0].thisBullet->setYCoord(game->tankPointer[0].thisBullet->getYCoord() + (game->tankPointer[0].thisBullet->getSpeed() * game->getScaleFactor() * (sin((game->tankPointer[0].thisBullet->getRotation()*M_PI) / 180)))); //y coord bullet movement with rotation

		//tank 2 bullet
	game->tankPointer[1].thisBullet->setXCoord(game->tankPointer[1].thisBullet->getXCoord() + (game->tankPointer[1].thisBullet->getSpeed() * game->getScaleFactor() * (cos((game->tankPointer[1].thisBullet->getRotation()*M_PI) / 180)))); //x coord bullet movement with rotation
	game->tankPointer[1].thisBullet->setYCoord(game->tankPointer[1].thisBullet->getYCoord() + (game->tankPointer[1].thisBullet->getSpeed() * game->getScaleFactor() * (sin((game->tankPointer[1].thisBullet->getRotation()*M_PI) / 180)))); //y coord bullet movement with rotation

	//tank 2 turret
	game->tankPointer[0].tankTurret.setXCoord(game->tankPointer[0].getXCoord() + (game->tankPointer[0].tankTurret.getSpeed() * game->getScaleFactor() * (cos((game->tankPointer[0].tankTurret.getRotation()*M_PI) / 180)))); //x coord turret movement with rotation
	game->tankPointer[0].tankTurret.setYCoord(game->tankPointer[0].getYCoord() + (game->tankPointer[0].tankTurret.getSpeed() * game->getScaleFactor() * (sin((game->tankPointer[0].tankTurret.getRotation()*M_PI) / 180)))); //y coord turret movement with rotation

	//tank 2 turret
	game->tankPointer[1].tankTurret.setXCoord(game->tankPointer[1].getXCoord() + (game->tankPointer[1].tankTurret.getSpeed() * game->getScaleFactor() * (cos((game->tankPointer[1].tankTurret.getRotation()*M_PI) / 180)))); //x coord turret movement with rotation
	game->tankPointer[1].tankTurret.setYCoord(game->tankPointer[1].getYCoord() + (game->tankPointer[1].tankTurret.getSpeed() * game->getScaleFactor() * (sin((game->tankPointer[1].tankTurret.getRotation()*M_PI) / 180)))); //y coord turret movement with rotation
}

void restart()
{

}