#include "instance.h"


instance::instance()
{
	noOfTanks = 2;
	idealFPS = 60;

	tankPointer = new tank[2];

	srand(time(NULL));

	float ran1 = rand() % 200;
	float ran2 = rand() % 200;
	float ran3 = rand() % 200;
	float ran4 = rand() % 200;
	float ran5 = rand() % 200;

	powerUp1 = new powerUp((ran1 - 100) * (2.6 / 100), (ran2 - 100) * (1.95 / 100), ran5 * (360 / 100));
	powerUp2 = new powerUp((ran3 - 100) * (2.6 / 100), (ran4 - 100) * (1.95 / 100));

	frameStartTime = 0;
	frameEndTime = 0;
	currentDelta = 0;
	scaleFactor = 0;
	idealDelta = 1000 / idealFPS;
}

instance::~instance()
{
	delete[] tankPointer;
	tankPointer = NULL;

	delete powerUp1;
	powerUp1 = NULL;

	delete powerUp2;
	powerUp2 = NULL;
}

void instance::calculateCurrentDelta(void)
{
	currentDelta = frameEndTime - frameStartTime;
}

void instance::calculateScaleFactor(void)
{
	scaleFactor = currentDelta / idealDelta;
}

int instance::getNoOfTanks(void)
{
	return noOfTanks;
}

void instance::setNoOfTanks(int num)
{
	noOfTanks = num;
}

DWORD instance::getFrameStartTime(void)
{
	return frameStartTime;
}

void instance::setFrameStartTime(DWORD start)
{
	frameStartTime = start;
}

DWORD instance::getFrameEndTime(void)
{
	return frameEndTime;
}

void instance::setFrameEndTime(DWORD end)
{
	frameEndTime = end;
}

float instance::getCurrentDelta(void)
{
	return currentDelta;
}

void instance::setCurrentDelta(float delta)

{
	currentDelta = delta;
}

float instance::getScaleFactor(void)
{
	return scaleFactor;
}

void instance::setScaleFactor(float sf)
{
	scaleFactor = sf;
}

float instance::getIDealDelta(void)
{
	return idealDelta;
}
