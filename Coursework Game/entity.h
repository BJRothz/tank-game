#pragma once
#include <string>

using namespace std;

class entity
{
public:

	entity();

	// get functions for variables
	float getXCoord(void);
	float getYCoord(void);
	float getRotation(void);

	// set functions for variables
	void setXCoord(float x);
	void setYCoord(float y);
	void setRotation(float theRotation);

protected:
	float xCoord;
	float yCoord;
	float rotation;

};