#include "tank.h"


tank::tank()
{
	xCoord = 0.0f;
	yCoord = 0.0f;
	rotation = 0.0f;
	tankTurret.setRotation(rotation);

	topSpeed = 0.02f;
	speed = 0.0f;
	acceleration = 0.0f;
	momentum = 0.0f;

	thisBullet = new bullet;

	moveForward = 0;
	moveBackward = 0;
	turnRight = 0;
	turnLeft = 0;
	fire = 0;
	reloaded = 1;
	invincible = 0;
}

tank::~tank()
{
	delete thisBullet;
	thisBullet = NULL;
}

float tank::getThisScaleFactor(void)
{
	return thisScaleFactor;
}

bool tank::getMoveForward(void)
{
	return moveForward;
}

bool tank::getMoveBackward(void)
{
	return moveBackward;
}

bool tank::getTurnRight(void)
{
	return turnRight;
}

bool tank::getTurnLeft(void)
{
	return turnLeft;
}

bool tank::getFire(void)
{
	return fire;
}

bool tank::getReloaded(void)
{
	return reloaded;
}

bool tank::getInvincible(void)
{
	return invincible;
}

void tank::setThisScaleFactor(float sf)
{
	thisScaleFactor = sf;
}

void tank::setMoveForward(bool state)
{
	moveForward = state;
}

void tank::setMoveBackward(bool state)
{
	moveBackward = state;
}

void tank::setTurnRight(bool state)
{
	turnRight = state;
}

void tank::setTurnLeft(bool state)
{
	turnLeft = state;
}

void tank::setFire(bool state)
{
	fire = state;
}

void tank::setReloaded(bool state)
{
	reloaded = state;
}

void tank::setInvincible(bool state)
{
	invincible = state;
}

//resulting changes due to key down events
void tank::control(void)
{
	if (moveForward && speed < topSpeed)
	{
		speed += 0.002f;
	}

	if (moveBackward && speed > -topSpeed)
	{
		speed -= 0.002f;
	}

	if (turnRight)
	{
		rotation -= 1.0f * thisScaleFactor;
	}

	if (turnLeft)
	{
		rotation += 1.0f * thisScaleFactor;
	}

	if (fire)
	{
		thisBullet->setXCoord(xCoord);
		thisBullet->setYCoord(yCoord);
		thisBullet->setRotation(tankTurret.getRotation());
		thisBullet->setSpeed(0.03f);

		reloaded = 0;
	}
}
