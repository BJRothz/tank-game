#pragma once
#include <string>
#include "entity.h"
#include "movingEntity.h"
#include "turret.h"
#include "bullet.h"

using namespace std;

class tank : public movingEntity
{
public:
	turret tankTurret;
	
	bullet * thisBullet = new bullet;

	tank();

	~tank();

	// get variables function prototypes
	float getThisScaleFactor(void);

	bool getMoveForward(void);
	bool getMoveBackward(void);
	bool getTurnRight(void);
	bool getTurnLeft(void);
	bool getFire(void);
	bool getReloaded(void);
	bool getInvincible(void);

	// set variables function prototypes
	void setThisScaleFactor(float sf);

	void setMoveForward(bool state);
	void setMoveBackward(bool state);
	void setTurnRight(bool state);
	void setTurnLeft(bool state);
	void setFire(bool state);
	void setReloaded(bool state);
	void setInvincible(bool state);

	// movement function prototype
	void control(void);

	// checks if the tank turret is reloaded
	void checkReloaded(void);

protected:
	float thisScaleFactor;

	bool moveForward;
	bool moveBackward;
	bool turnRight;
	bool turnLeft;
	bool fire;
	bool reloaded;
	bool invincible;
};