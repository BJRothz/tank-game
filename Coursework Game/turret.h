#pragma once
#include <string>
#include "entity.h"
#include "movingEntity.h"

using namespace std;

class turret : public movingEntity
{
public:
	turret();

	//get functions
	bool getRotateLeft(void);
	bool getRotateRight(void);

	float getThisScaleFactor(void);

	//set functions
	void setRotateLeft(bool state);
	void setRotateRight(bool state);

	void setThisScaleFactor(float sf);

	//controls turret rotation
	void turretRotation(void);

protected:

	bool rotateLeft;
	bool rotateRight;

	float thisScaleFactor;
};